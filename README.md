# 

# Angular Netlify Example

## Description

`angular-netlify-example`` is meant to explore and demonstrate how to setup a CI/CD pipeline for a simple angular project employing gitlab and netlify.

The initial version owes important inspiration to an article on Medium (https://lakshyajit165.medium.com/deploying-an-angular-app-to-netlify-using-netlify-cli-and-gitlab-ci-313d9ff9180a)

## Tasks 

1. Install @angular/cli (if you haven't done so alreay, see: https://angular.io/guide/setup-local): `npm install -g @angular/cli`
1. Create a new Angular project: `ng new angular-netlify-example` (Use default settings in the dialogue)
1. Install the Netlify CLI: `npm install -g netlify-cli`
1. Use the netlify-cli to create and configure a new site: `netlify init --manual` (will launch the browser on the first run to authorize `netlify-cli` for Netlify) - use defaults - newly created site-id will be stored in a file named `state.json` in the (hidden) `.netlify` directory
1. In Netlify's user settings (https://app.netlify.com/user/applications) create a new personal access token (PAT) which we will use for deployment automation.
1. Create a file named `netlify.toml` in the root of `angular-netlify-example` and configure as follows: 
   ```toml
   [build]
     publish = "dist"
   ```
   > This will instruct Netlify to use the `dist` folder as the base for the app to be published
1. Create a new gitlab project named `angular-netlify-example` and set this project as the local `angular-netlify-example`'s remote, e.g: `git remote add origin git@gitlab.com:chrberndt/angular-netlify-example`
1. Setup `NETLIFY_AUTH_TOKEN` and `NETLIFY_SITE_ID` in the CI/CD section of your gitlab project (Settings -> CI/CD -> Variables) (`NETLIFY_AUTH_TOKEN` from step ??; `NETLIFY_SITE_ID` from `.netlify/state.json`) - use default visibility settings for variables
1. Setup a `.gitlab-ci.yml` file to trigger a build once we push our code (for a detailed documentation see: https://docs.gitlab.com/ee/ci/)

## TODO: Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## TODO: Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## TODO: Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## TODO: Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## TODO: Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## TODO: Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## TODO: Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## TODO: Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## TODO: License
For open source projects, say how it is licensed.

## TODO: Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

## TODO: Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

# AngularNetlifyExample

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.0.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
